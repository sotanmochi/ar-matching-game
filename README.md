# AR Matching Game

地域に人を集めよう！

地域には広い空き地やだだっ広い場所があると思います。そんな何も無い場所だからこそできることをしましょう。

大きなカードで神経衰弱をしませんか？

スマホがあればだだっ広い場所一面にカードを並べて、動き回りながらカードをめくる神経衰弱ができます。地域興しのイベントやお祭りで誰もが知っている神経衰弱というゲームで人を集めましょう。

![Video in Imgur ](./video/ARMatchingGame_demo.mp4)

## 使い方
- ゲームはAR機能が動作するスマホで遊べます。
- ゲーム参加者の一人が代表となります。代表をオーナーと呼びます。
- オーナーがルームを作ります。オーナー以外の参加者はルームに参加します。
- オーナーがカードの枚数を指定します。
- 参加人数のプレイヤーがルームに入った後、オーナーがゲームをスタートします。順番は自動で決まります。
- プレイヤーは自分の番になると自分が立っているカードをめくれます。
- プレイヤーは上空から俯瞰したような図で他のプレイヤーがめくった状況を見れます。自分の位置も分かります。
- 全員がめくったカードを確認するまで待ちます。　確認は"OK"ボタンをおします。
- 参加者は全員とボイスチャットができます。


## 神経衰弱ゲームの仕様
- ゲームの終了判定は、場からすべてのカードが消えたときです。
- プレイヤーのターンの終了判定は、2枚カードをめくり、数字がそろっていないときです。


## 環境

### Unityバージョン

2020.3.0f1

### 必要なアセット

[Free Playing Cards Pack](https://assetstore.unity.com/packages/3d/props/tools/free-playing-cards-pack-154780)  
[UniRx - Reactive Extensions for Unity](https://assetstore.unity.com/packages/tools/integration/unirx-reactive-extensions-for-unity-17276)  
[Photon Unity Networking 2](https://assetstore.unity.com/packages/tools/network/pun-2-free-119922)  
  --> 開発者は、PhotonのアプリIDを取得してゲームビルド時に設定してください。  

### 必要なSDK
[Vivox](https://unity.com/ja/products/vivox)  
  --> 開発者は、Vivoxのアカウント作成後Vivoxアプリ作成し発行者IDとシークレットキーを取得します。VivoxSDKをUnityに導入し、VenueSceneとVenue3DSceneのヒエラルキーメニュー内のVivoxVoiceManagerのtokerissuerに発行者IDを、tokenkeyにシークレットキーを記入し、ゲームをビルドしてください。

### 開発支援ツール

- ドキュメントは[plantuml](https://plantuml.com/ja/) を利用しています。
- [editorconfig](https://www.neputa-note.net/2020/10/vscode-editorconfig.html
)の「editorconfigを使えるようにする」の章に従って導入してください。


## 設計

### UI

https://www.figma.com/file/V3FzEx22ZShtzS8zQc3VbY/proto?node-id=0%3A1  
プレビュー：https://www.figma.com/proto/V3FzEx22ZShtzS8zQc3VbY/proto?node-id=1%3A3&scaling=scale-down

### クラス図

[クラス図](./doc/クラス図.pu)


### アーキテクチャ

MV(R)Pを採用しています。

[アーキテクチャ図](./doc/アーキテクチャ.pu)

### ゲームフロー図

[ゲームフロー図](./doc/ゲーム全体の流れ.pu)

### リファクタリング案

[リファクタリング後クラス図](./doc/クラス図_リファクタリング案.pu)  
[リファクタリング後アーキテクチャ図](./doc/アーキテクチャ_リファクタリング案.pu)


## 規約

### コーディング規約
[editorconfig](https://editorconfig.org/)を利用しています。  
[設定ファイル](./.editorconfig)

### クラス作成規約

- Presenterはシーン毎で作成
- ModelはPureCSharp & Immutable
