using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MatchingGame
{
    namespace Common
    {
        public class TrumpProperty : MonoBehaviour
        {
            public TrumpNumber TrumpNumber { get; private set; }
            public TrumpType TrumpType { get; private set; }

            private bool _IsSetProperty;
            // Start is called before the first frame update
            public bool SetProperty(TrumpType trumpType, TrumpNumber trumpNumber)
            {
                if (!_IsSetProperty)
                {
                    TrumpType = trumpType;
                    TrumpNumber = trumpNumber;
                    _IsSetProperty = !_IsSetProperty;
                    return true;
                }
                else
                {
                    Debug.Log("It's prohibited to call SetProperty twice. Aready Set." + "TrumpType; " + TrumpType + "TrumpNumber: " + trumpNumber);
                    return false;
                }
                
            }

            void Awake()
            {
                _IsSetProperty = false;
            }

        }
    }
}