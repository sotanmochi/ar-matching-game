using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MatchingGame
{
    namespace Common
    {
        public class AvatarContainer : MonoBehaviour
        {
            private List<AvatarContainerChild> _AvatarList = new List<AvatarContainerChild>();

            public AvatarContainerChild this[int index] => _AvatarList[index];
            public int Count => _AvatarList.Count;

            private void OnTransformChildrenChanged()
            {
                _AvatarList.Clear();
                foreach (Transform child in transform)
                {
                    _AvatarList.Add(child.GetComponent<AvatarContainerChild>());
                }
            }
        }
    }
}
