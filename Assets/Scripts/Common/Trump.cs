using System.Collections;
using System.Collections.Generic;

namespace MatchingGame
{
    namespace Common
    {
        public enum TrumpType
        {
            CLOVER,
            SPADE,
            DIAMOND,
            HEART,
            JOKER,
        };

        public enum TrumpNumber
        {
            TWO,
            THREE,
            FOUR,
            FIVE,
            SIX,
            SEVEN,
            EIGHT,
            NINE,
            TEN,
            JACK,
            QUEEN,
            KING,
            ACE,
        };

        /// <summary>
        /// トランプ構造体
        /// </summary>
        public struct Trump
        {
            public TrumpType TrumpType { get; }
            public TrumpNumber TrumpNumber { get; }

            /// <summary>トランプ情報</summary>
            /// <param name="trumpType">TrumpType</param>
            /// <param name="trumpNumber">TrumpNumber</param>
            public Trump(
                TrumpType trumpType,
                TrumpNumber trumpNumber
            )
            {
                TrumpType = trumpType;
                TrumpNumber = trumpNumber;
            }
        }
    }
}

