using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

namespace MatchingGame
{
    namespace Common
    {
        public class AvatarContainerChild : MonoBehaviourPunCallbacks
        {
            public Player Owner => photonView.Owner;

            public bool IsMine => photonView.IsMine;

            public override void OnEnable()
            {
                base.OnEnable();

                var container = FindObjectOfType<AvatarContainer>();
                transform.SetParent(container.transform);
            }
        }
    }
}