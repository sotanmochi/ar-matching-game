using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Android;
using UniRx;
using MatchingGame.View;
using MatchingGame.Model;
using MatchingGame.Common;
using VContainer;
using VContainer.Unity;
using Photon.Pun;
using VivoxUnity;

namespace MatchingGame
{
    namespace Presenter
    {
        public class VenuePresenter : IStartable, ITickable, Photon.Realtime.IMatchmakingCallbacks
        {
            private const string PLAYER_PIN_ON_MAP_PREFAB_NAME = "PlayerPinOnMap";

            [Inject] private IARView _ArView;
            [Inject] DeckView _DeckView;
            [Inject] ButtonView _ButtonView;
            [Inject] MapButtonView _MapButtonView;
            [Inject] ReturnToVenueButtonView _ReturnToVenueButton;
            [Inject] GameObject _MapCamera;
            [Inject] OKView _OKView;
            [Inject] ScoreTextView _ScoreTextView;
            [Inject] AvatarContainer _AvatarContainer;
            [Inject] PlayerStatusPanelView _PlayerStatusPanelView;
            [Inject] RPCView _RPCView;
            [Inject] VoiceButtonView _VoiceButtonView;

            private PlayerPinOnMapView _PlayerPinOnMap;

            /// <summary>
            /// ゲームモード. 外から設定するためにPublicになっている。
            /// </summary>
            public static GameMode GameMode { get; set; }


            /// <summary>
            /// MatchingGame状態
            /// </summary>
            private MatchingGameState _MatchingGameState;

            /// <summary>
            /// プレイヤー
            /// ToDO 将来的にEntrancePresenterからPlayer配列をここに設定することになる。
            /// </summary>
            public Player[] Players;

            private IDisposable _TouchPlaneDisposable;
            private List<IDisposable> _CardTriggerEnterDisposables = new List<IDisposable>();
            private List<IDisposable> _CardTriggerExitDisposables = new List<IDisposable>();
            private IDisposable _ButtonClickObservable;
            private IDisposable _MapButtonClickObservable;
            private IDisposable _ReturnToVenueButtonClickObservable;
            private IDisposable _OKClickObservable;
            private IDisposable _VoiceButtonClickObservable;
            private IDisposable _RPCReadyObservable;
            private IDisposable _RPCReceiveDeckObservable;
            private IDisposable _RPCFaceUpObservable;
            private IDisposable _RPCConfirmObservable;

            // マップ表示中を表すフラグ
            private bool _IsMapMode;
            // 初回Updateメソッド呼び出しを判定する
            bool _IsFirst = true;
            // 自身のプレイヤーID
            private int _OwnPlayerId;

            // カードを表示する準備ができたプレイヤーの人数
            private int _ReadPlayerCount = 0;
            // めくったカードの確認が終わったプレイヤーの人数
            private int _ConfirmedPlayerCount = 0;
            private Vector3 _DeckPosition;

            private VivoxVoiceManager _VivoxVoiceManager;
            private bool _PermissionsDenied;

            void IStartable.Start()
            {
                _TouchPlaneDisposable = _ArView.TouchPlaneObservable.Subscribe(position => ReadyMyself(position));
                _ButtonClickObservable = _ButtonView.ButtonClickObservable.Subscribe(gameObject => FaceUp(gameObject));
                _MapButtonClickObservable = _MapButtonView.ButtonClickObservable.Subscribe(x => ShowMap());
                _ReturnToVenueButtonClickObservable = _ReturnToVenueButton.ButtonClickObservable.Subscribe(x => HideMap());
                _OKClickObservable = _OKView.OKButtonClickObservable.Subscribe(_ => _RPCView.Confirm());
                _VoiceButtonClickObservable = _VoiceButtonView.VoiceButtonClickObservable.Subscribe(isEnable => SwitchMicrophone(isEnable));
                _RPCReadyObservable = _RPCView.ReadyObservable.Subscribe(playerID => Ready(playerID));
                _RPCReceiveDeckObservable = _RPCView.ReceiveDeckObservable.Subscribe(deck => ReceieDeck(deck));
                _RPCFaceUpObservable = _RPCView.FaceUpObservable.Subscribe(trump => FaceUp(trump));
                _RPCConfirmObservable = _RPCView.ConfirmObservable.Subscribe(playerId => Confirm(playerId));
                _ButtonView.Hide(); // buttonViewのAwake関数を呼び出したいため、ここで非表示処理を行う。Unityの設定で初期状態を非表示にしているとSetActive(true)が呼ばれるまでAwake関数が呼び出されず、Subscribeがうまくいかない。
                _ReturnToVenueButton.Hide();
                _OKView.Hide();
                _VoiceButtonView.Hide();
                _PlayerStatusPanelView.ClearAllStatus();
                _IsMapMode = false;

                _OwnPlayerId = PhotonNetwork.LocalPlayer.ActorNumber;
                foreach (Photon.Realtime.Player player in PhotonNetwork.PlayerList)
                {
                    _PlayerStatusPanelView.AddPlayer(player.NickName, player.ActorNumber);
                }

                // マスターであればデッキを生成する
                if (PhotonNetwork.IsMasterClient)
                {
                    _MatchingGameState = MatchingGameFunction.Create(GetPayerList().ToArray(), GameMode);
                    Debug.Log(ModelUtils.LogStringMatchingGameState(_MatchingGameState));
                    _DeckView.Initialize(GameMode, _MatchingGameState.Deck.Cards.Select(c => c.Trump));

                    // DeckViewから生成したカードモデルのCardViewを受けとってSubscribe
                    foreach (GameObject o in _DeckView.Cards)
                    {
                        CardView cardview = o.GetComponent<CardView>();
                        IDisposable disposable = cardview.CardTriggerEnterObservable.Subscribe(gameObject => ShowFaceUpButton(gameObject));
                        _CardTriggerEnterDisposables.Add(disposable);

                        disposable = cardview.CardTriggerExitObservable.Subscribe(gameObject => HideFaceUpButton(gameObject));
                        _CardTriggerExitDisposables.Add(disposable);
                    }
                }

                // ネットワーク上で共有するオブジェクトの生成
                CreatePlayerPinOnMap();
                _PlayerPinOnMap.Hide();

                PhotonNetwork.AddCallbackTarget(this);
                
                _VivoxVoiceManager = VivoxVoiceManager.Instance;
                _VivoxVoiceManager.InitializeVivox();
                _VivoxVoiceManager.OnUserLoggedInEvent += OnUserLoggedIn;
            }

            void ITickable.Tick()
            {
                if (_VivoxVoiceManager.LoginState == LoginState.LoggedOut)
                {
                    if (Permission.HasUserAuthorizedPermission(Permission.Microphone))
                    {
                        // The user authorized use of the microphone.
                        _VivoxVoiceManager.Login(PhotonNetwork.LocalPlayer.ActorNumber.ToString());
                    }
                    else
                    {
                        // Check if the users has already denied permissions
                        if (_PermissionsDenied)
                        {
                            _PermissionsDenied = false;
                            _VivoxVoiceManager.Login(PhotonNetwork.LocalPlayer.ActorNumber.ToString());
                        }
                        else
                        {
                            _PermissionsDenied = true;
                            // We do not have permission to use the microphone.
                            // Ask for permission or proceed without the functionality enabled.
                            Permission.RequestUserPermission(Permission.Microphone);
                        }
                    }
                }
            }

            /**
                * デッキを表示する準備が整ったことを表す
                * 1回平面を検知したら平面検知のオブザーバを解除する。
                */
            private void ReadyMyself(Vector3 deckPosition)
            {
                _DeckPosition = deckPosition;
                _PlayerPinOnMap.transform.position = new Vector3(_PlayerPinOnMap.transform.position.x, deckPosition.y + 0.1f, _PlayerPinOnMap.transform.position.z); //デッキより少し上に設定
                _RPCView.Ready();
                _TouchPlaneDisposable?.Dispose();
            }

            /**
             * 全プレイヤーの準備が整ったらデッキを送信し、表示する。（マスターのみ）
             */
            private void Ready(int playerID)
            {
                _ReadPlayerCount++;
                _PlayerStatusPanelView.UpdateStatus(playerID, "Ready");
                if (_ReadPlayerCount == PhotonNetwork.PlayerList.Length && PhotonNetwork.IsMasterClient)
                {
                    _DeckView.ShowDeck(_DeckPosition);
                    _PlayerStatusPanelView.Hide();
                    _RPCView.SendDeck(_MatchingGameState.Deck);
                }
            }

            /**
             * マスターから受け取ったデッキを表示する
             */
            private void ReceieDeck(Deck deck)
            {
                // ToDO 綺麗な直し方にする
                switch (deck.Horizontal)
                {
                    case 2: GameMode = GameMode.MODE_2x2; break;
                    case 4: GameMode = GameMode.MODE_4x4; break;
                    case 8: GameMode = GameMode.MODE_4x8; break;
                    default: GameMode = GameMode.MODE_4x13; break;
                }
                _MatchingGameState = MatchingGameFunction.Create(GetPayerList().ToArray(), deck);
                Debug.Log(ModelUtils.LogStringMatchingGameState(_MatchingGameState));
                _DeckView.Initialize(GameMode, _MatchingGameState.Deck.Cards.Select(c => c.Trump));

                // DeckViewから生成したカードモデルのCardViewを受けとってSubscribe
                foreach (GameObject o in _DeckView.Cards)
                {
                    CardView cardview = o.GetComponent<CardView>();
                    IDisposable disposable = cardview.CardTriggerEnterObservable.Subscribe(gameObject => ShowFaceUpButton(gameObject));
                    _CardTriggerEnterDisposables.Add(disposable);

                    disposable = cardview.CardTriggerExitObservable.Subscribe(gameObject => HideFaceUpButton(gameObject));
                    _CardTriggerExitDisposables.Add(disposable);
                }

                _DeckView.ShowDeck(_DeckPosition);
                _PlayerStatusPanelView.Hide();
            }

            private void OnDestroy()
            {
                PhotonNetwork.RemoveCallbackTarget(this);

                _TouchPlaneDisposable?.Dispose();
                _ButtonClickObservable?.Dispose();
                _MapButtonClickObservable?.Dispose();
                _ReturnToVenueButtonClickObservable?.Dispose();
                _OKClickObservable?.Dispose();
                _RPCReadyObservable?.Dispose();
                _RPCReceiveDeckObservable?.Dispose();
                _RPCFaceUpObservable?.Dispose();
                _RPCConfirmObservable?.Dispose();
                _VoiceButtonClickObservable?.Dispose();

                foreach (IDisposable disposable in _CardTriggerEnterDisposables)
                {
                    disposable?.Dispose();
                }
                foreach (IDisposable disposable in _CardTriggerExitDisposables)
                {
                    disposable?.Dispose();
                }

            }

            private void ShowFaceUpButton(GameObject cardObject)
            {
                // マップ表示中はめくれない
                if (_IsMapMode)
                {
                    return;
                }

                // 自分の番でなければめくれない
                if (!MatchingGameFunction.CanFaceUp(_MatchingGameState, _OwnPlayerId))
                {
                    return;
                }

                TrumpProperty p = cardObject.GetComponent<TrumpProperty>();
                if (_MatchingGameState.Turn.Trump?.TrumpNumber == p.TrumpNumber
                    && _MatchingGameState.Turn.Trump?.TrumpType == p.TrumpType)
                {
                    // すでにFaceup済み
                    return;
                }

                _ButtonView.Show(cardObject.GetComponent<CardView>());
            }

            private void HideFaceUpButton(GameObject _)
            => _ButtonView.Hide();

            // 自分でカードをめくった時の挙動
            private void FaceUp(GameObject cardGameObject)
            {
                cardGameObject.GetComponent<CardView>().FaceUp();
                HideFaceUpButton(cardGameObject);
                TrumpProperty trumpProperty = cardGameObject.GetComponent<TrumpProperty>();
                Debug.Log("Faceup TrumpType: " + trumpProperty.TrumpType + "TrumpNumber: " + trumpProperty.TrumpNumber);
                TryNum previousState = _MatchingGameState.Turn.TryNum;
                // ToDO Playerを更新
                _MatchingGameState = MatchingGameFunction.FaceUp(_MatchingGameState, _MatchingGameState.Turn.Player, new Trump(trumpProperty.TrumpType, trumpProperty.TrumpNumber));

                if (previousState == TryNum.TRY_2ND) {
                    // めくったカードを確認するフローに入る
                    ShowMap();
                    _OKView.Show();
                    _ReturnToVenueButton.Hide();
                    _PlayerStatusPanelView.ClearAllStatus();
                    _PlayerStatusPanelView.Show();
                    _ConfirmedPlayerCount = 0;
                }
                _RPCView.FaceUp(trumpProperty);
            }

            // 他の人がカードをめくった時の挙動
            private void FaceUp(Trump trump)
            {
                _DeckView.FaceUp(trump);

                TryNum previousState = _MatchingGameState.Turn.TryNum;
                // ToDO Playerを更新
                _MatchingGameState = MatchingGameFunction.FaceUp(_MatchingGameState, _MatchingGameState.Turn.Player, trump);

                if (previousState == TryNum.TRY_2ND)
                {
                    // めくったカードを確認するフローに入る
                    ShowMap();
                    _OKView.Show();
                    _ReturnToVenueButton.Hide();
                    _PlayerStatusPanelView.ClearAllStatus();
                    _PlayerStatusPanelView.Show();
                    _ConfirmedPlayerCount = 0;
                }
            }

            public void Confirm(int playerId)
            {
                _PlayerStatusPanelView.UpdateStatus(playerId, "OK");
                _ConfirmedPlayerCount++;
                if (_ConfirmedPlayerCount == PhotonNetwork.PlayerList.Length)
                {
                    _PlayerStatusPanelView.Hide();
                    ContinueProcess();
                }
            }

            private void ContinueProcess() {
                _DeckView.UpdateCardActiveState(_MatchingGameState.Deck.Cards.Where(c => c.Player == null).Select(c => c.Trump));
                // すべてFaceDown
                _DeckView.Cards.ForEach(c => {
                    c.GetComponent<CardView>().FaceDown();
                });

                // ゲーム終了判定
                if (MatchingGameFunction.IsEndGame(_MatchingGameState))
                {
                    Debug.Log("Game is Over");
                    _VivoxVoiceManager.DisconnectAllChannels();
                    _VivoxVoiceManager.Logout();
                    PhotonNetwork.LeaveRoom();
                    // Roomから離脱してからResultSceneを表示する。
                } else
                {
                    _ScoreTextView.UpdateScore(MatchingGameFunction.GetPlayerScore(_MatchingGameState, _OwnPlayerId));
                }

                HideMap();
            }

            private void ShowMap()
            {
                // UIの表示・非表示
                _MapButtonView.Hide();
                _ReturnToVenueButton.Show();
                _ButtonView.Hide();

                // Map用のカメラの設定
                _MapCamera.SetActive(true);
                _MapCamera.transform.position = _DeckView.DeckCenterPosition;
                _MapCamera.transform.position += new Vector3(0, 17f, 0);

                // マップ上に全プレイヤーを表示
                for (int i = 0; i < _AvatarContainer.Count; i++)
                {
                    _AvatarContainer[i].GetComponent<PlayerPinOnMapView>().Show();
                }


                // フラグの更新
                _IsMapMode = true;
            }

            private void HideMap()
            {
                // UIの表示・非表示
                _MapButtonView.Show();
                _ReturnToVenueButton.Hide();
                _OKView.Hide();

                // Map用のカメラの設定
                _MapCamera.SetActive(false);

                // マップ上に全プレイヤーを非表示
                for (int i = 0; i < _AvatarContainer.Count; i++)
                {
                    _AvatarContainer[i].GetComponent<PlayerPinOnMapView>().Hide();
                }

                // フラグの更新
                _IsMapMode = false;
            }

            private void CreatePlayerPinOnMap()
            {
                _PlayerPinOnMap = PhotonNetwork.Instantiate(PLAYER_PIN_ON_MAP_PREFAB_NAME, new Vector3(0, 0, 0), Quaternion.identity, 0).GetComponent<PlayerPinOnMapView>();
                _PlayerPinOnMap.Player = _ArView;
                Vector3 playerPosition = _ArView.GetPlayerPosition();
                _PlayerPinOnMap.transform.position = new Vector3(playerPosition.x, 0, playerPosition.z); 
            }

            private List<Player> GetPayerList()
            {
                List<Player> list = new List<Player>();
                foreach (Photon.Realtime.Player player in PhotonNetwork.PlayerList)
                {
                    list.Add(new Player(player.NickName, player.ActorNumber));
                }
                return list.OrderBy(p => p.PlayerId).ToList<Player>(); // TODO ランダムで順番を決めるようにする
            }

            private void ResultPresenterSceneLoaded(Scene next, LoadSceneMode mode)
            {
                // TODO 一時的対応
                // なぜかresultPresenterのインスタンスがロードされたものと異なることがある。
                // この原因を究明する
                //ResultPresenter resultPresenter = GameObject.Find("ResultPresenter").GetComponent<ResultPresenter>();
                ResultPresenter.Results = MatchingGameFunction.GetResutls(_MatchingGameState);

                // イベントから当メソッドを削除
                SceneManager.sceneLoaded -= ResultPresenterSceneLoaded;
            }

            private void OnUserLoggedIn()
            {
                // チャンネルに入る
                Debug.Log("Photon room name = " + PhotonNetwork.CurrentRoom.Name);
                _VivoxVoiceManager.JoinChannel(PhotonNetwork.CurrentRoom.Name, ChannelType.NonPositional, VivoxVoiceManager.ChatCapability.AudioOnly);

                _VoiceButtonView.Show();
            }

            private void SwitchMicrophone(bool isEnable)
            {
                _VoiceButtonView.SwitchMicrophone();
                _VivoxVoiceManager.AudioInputDevices.Muted = isEnable;
            }

            public void OnFriendListUpdate(List<Photon.Realtime.FriendInfo> friendList) { }
            public void OnCreatedRoom() { }
            public void OnCreateRoomFailed(short returnCode, string message) { }
            public void OnJoinedRoom() { }
            public void OnJoinRoomFailed(short returnCode, string message) { }
            public void OnJoinRandomFailed(short returnCode, string message) { }
            public void OnLeftRoom()
            {
                SceneManager.sceneLoaded += ResultPresenterSceneLoaded;
                SceneManager.LoadScene("ResultScene");
            }
        }
    }
}

