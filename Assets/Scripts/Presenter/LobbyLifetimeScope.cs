using VContainer;
using VContainer.Unity;
using MatchingGame.View;
using UnityEngine;

namespace MatchingGame
{
    namespace Presenter
    {
        public class LobbyLifetimeScope : LifetimeScope
        {
            [SerializeField] JoinMenberPanelView _JoinMenberPanelView;
            [SerializeField] ConnectLabelView _ConnectLabelView;
            [SerializeField] JoinButtonView _JoinButtonView;
            [SerializeField] NameInputFieldView _NameInputFieldView;
            [SerializeField] StartButtonView _StartButtonView;
            [SerializeField] ExitButtonView _ExitButtonView;

            protected override void Configure(IContainerBuilder builder)
            {
                builder.RegisterComponent<JoinMenberPanelView>(_JoinMenberPanelView);
                builder.RegisterComponent<ConnectLabelView>(_ConnectLabelView);
                builder.RegisterComponent<JoinButtonView>(_JoinButtonView);
                builder.RegisterComponent<NameInputFieldView>(_NameInputFieldView);
                builder.RegisterComponent<StartButtonView>(_StartButtonView);
                builder.RegisterComponent<ExitButtonView>(_ExitButtonView);

                builder.RegisterEntryPoint<LobbyPresenter>(Lifetime.Scoped);
            }
        }
    }
}