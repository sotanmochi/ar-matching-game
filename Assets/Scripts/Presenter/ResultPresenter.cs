using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MatchingGame.Model;
using MatchingGame.View;
using UnityEngine.UI;
using UniRx;

public class ResultPresenter : MonoBehaviour
{
    public static IEnumerable<Result> Results { get; set; }

    [SerializeField] List<GameObject> _ScorePanels;
    [SerializeField] NewGameButtonView _NewGameButtonView;

    private IDisposable _NewGameButtonClickObservable;

    // Start is called before the first frame update
    void Start()
    {
        _NewGameButtonClickObservable = _NewGameButtonView.ButtonClickObservable.Subscribe(x => LoadEntranceScene());

        // Scoreが高い順にソート
        var sortedResults = Enumerable.ToList<Result>(Results);
        sortedResults.Sort((a, b) => b.Score - a.Score);

        // 結果の表示
        for (int i = 0; i < 4; i++)
        {
            if (sortedResults.Count > i)
            {
                _ScorePanels[i].transform.Find("NameText").GetComponent<Text>().text = sortedResults[i].Player.PlayerName;
                _ScorePanels[i].transform.Find("ScoreText").GetComponent<Text>().text = sortedResults[i].Score + "pt";
            } else
            {
                _ScorePanels[i].SetActive(false);
            }
            
        }

    }

    private void LoadEntranceScene()
    {
        SceneManager.LoadScene("EntranceScene");
    }
    private void OnDestroy()
    {
        _NewGameButtonClickObservable?.Dispose();
    }

}
