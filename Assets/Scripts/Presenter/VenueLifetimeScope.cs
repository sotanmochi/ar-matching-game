using VContainer;
using VContainer.Unity;
using MatchingGame.View;
using MatchingGame.Common;
using UnityEngine;

namespace MatchingGame
{
    namespace Presenter
    {
        public class VenueLifetimeScope : LifetimeScope
        {

            [SerializeField] DeckView _DeckView;
            [SerializeField] ButtonView _ButtonView;
            [SerializeField] MapButtonView _MapButtonView;
            [SerializeField] ReturnToVenueButtonView _ReturnToVenueButton;
            [SerializeField] GameObject _MapCamera;
            [SerializeField] OKView _OKView;
            [SerializeField] ScoreTextView _ScoreTextView;
            [SerializeField] AvatarContainer _AvatarContainer;
            [SerializeField] PlayerStatusPanelView _PlayerStatusPanelView;
            [SerializeField] RPCView _RPCView;
            [SerializeField] VoiceButtonView _VoiceButtonView;

            protected override void Configure(IContainerBuilder builder)
            {
                builder.RegisterComponent<DeckView>(_DeckView);
                builder.RegisterComponent<ButtonView>(_ButtonView);
                builder.RegisterComponent<MapButtonView>(_MapButtonView);
                builder.RegisterComponent<ReturnToVenueButtonView>(_ReturnToVenueButton);
                builder.RegisterComponent<GameObject>(_MapCamera);
                builder.RegisterComponent<OKView>(_OKView);
                builder.RegisterComponent<ScoreTextView>(_ScoreTextView);
                builder.RegisterComponent<AvatarContainer>(_AvatarContainer);
                builder.RegisterComponent<PlayerStatusPanelView>(_PlayerStatusPanelView);
                builder.RegisterComponent<RPCView>(_RPCView);
                builder.RegisterComponent<VoiceButtonView>(_VoiceButtonView);
                builder.RegisterComponentInHierarchy<ARView>().AsImplementedInterfaces();

                builder.RegisterEntryPoint<VenuePresenter>(Lifetime.Scoped);
            }
        }
    }
}
