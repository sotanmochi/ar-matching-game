using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using MatchingGame.Common;


namespace MatchingGame
{
    namespace View
    {
        public class DeckView : MonoBehaviour
        {
            private const float SCALE = 3f;

            [SerializeField] GameObject _CardPrefab;

            private bool _Initialized;

            public List<GameObject> Cards { get; private set; }

            public Vector3 DeckCenterPosition { get; private set; }


            public void ShowDeck(Vector3 position)
            {
                // TODO カード情報を受け取って、指定された枚数表示する
                transform.position = position;
                gameObject.SetActive(true);
            }

            public void FaceUp(Trump trump)
            {
                foreach(GameObject c in Cards)
                {
                    if (c.GetComponent<TrumpProperty>().TrumpType == trump.TrumpType && 
                        c.GetComponent<TrumpProperty>().TrumpNumber == trump.TrumpNumber)
                    {
                        c.GetComponent<CardView>().FaceUp();
                    }
                }
            }

            void Awake()
            {
                _Initialized = false;
            }

            public bool Initialize(GameMode gameMode, IEnumerable<Trump> trumps)
            {
                if (!_Initialized)
                {
                    _Initialized = !_Initialized;
                    InitializeCards(gameMode, trumps);
                    return true;
                }
                throw new InvalidProgramException("既に初期化されています");
            }
            private bool InitializeCards(GameMode gameMode, IEnumerable<Trump> trumps)
            {
                var dimension = CommonFunction.GetCardDimension(gameMode);
                var dimensions = Enumerable.Range(0, trumps.Count()).Select(i => (i / dimension.Item2, i % dimension.Item2));
                Cards = new List<GameObject>();
                trumps.Zip(dimensions, (t, d) => (t, d)).ToList().ForEach(zipped =>
                {
                    GameObject card = Instantiate(
                        _CardPrefab,
                        new Vector3(SCALE * zipped.d.Item2, 0.1f, SCALE * zipped.d.Item1),
                        Quaternion.Euler(180.0f, 0f, 0f)
                    );
                    Debug.Log(card);
                    card.transform.parent = transform; // 親に設定
                    card.transform.localScale = new Vector3(0.1f, 1f, 0.1f);
                    Material material = GetMaterial(zipped.t);
                    //Debug.Log(material);
                    card.GetComponent<Renderer>().material = material;
                    card.GetComponent<TrumpProperty>().SetProperty(zipped.t.TrumpType, zipped.t.TrumpNumber);
                    Cards.Add(card);
                    Debug.Log("TrumpType: " + zipped.t.TrumpType + " TrumpNumber: " + zipped.t.TrumpNumber);

                });

                DeckCenterPosition = transform.position + new Vector3(SCALE * dimension.Item2 / 2, 0, SCALE * dimension.Item1 / 2);
                return true;
            }

            /// <summary>
            /// デッキの表示を更新
            /// </summary>
            /// <param name="trumps">画面に表示すべきカード</param>
            public void UpdateCardActiveState(IEnumerable<Trump> trumps)
            {
                trumps.ToList().ForEach(t =>
                {
                    Debug.Log("TrumpType: " + t.TrumpType + " TrumpNumber: " + t.TrumpNumber);
                });

                Cards.ForEach(c =>
                {
                    TrumpProperty p = c.GetComponent<TrumpProperty>();
                    
                    if (!trumps.ToList().Contains(new Trump(p.TrumpType, p.TrumpNumber)))
                    {
                        c.SetActive(false);
                    }
                    
                });
                return;
            }

            Material GetMaterial(Trump trump)
            {
                return Resources.Load<Material>(GetMaterialPath(trump));
            }

            string GetMaterialPath(Trump trump)
            {
                return "Materials/BackColor_Red/Red_PlayingCards_" 
                    + GetTrumpType(trump.TrumpType) 
                    + GetTrumpNumber(trump.TrumpNumber) 
                    + "_00";
            }

            string GetTrumpNumber(TrumpNumber number)
            {
                switch (number)
                {
                    case (TrumpNumber.TWO):     return "02";
                    case (TrumpNumber.THREE):   return "03";
                    case (TrumpNumber.FOUR):    return "04";
                    case (TrumpNumber.FIVE):    return "05";
                    case (TrumpNumber.SIX):     return "06";
                    case (TrumpNumber.SEVEN):   return "07";
                    case (TrumpNumber.EIGHT):   return "08";
                    case (TrumpNumber.NINE):    return "09";
                    case (TrumpNumber.TEN):     return "10";
                    case (TrumpNumber.JACK):    return "11";
                    case (TrumpNumber.QUEEN):   return "12";
                    case (TrumpNumber.KING):    return "13";
                    case (TrumpNumber.ACE):     return "01";
                    default: throw new ArgumentException("不正な引数");
                }
            }
            string GetTrumpType(TrumpType type)
            {
                switch (type)
                {
                    case (TrumpType.CLOVER):  return "Club";
                    case (TrumpType.SPADE):   return "Spade";
                    case (TrumpType.DIAMOND): return "Diamond";
                    case (TrumpType.HEART):   return "Heart";
                    default: throw new ArgumentException("不正な引数");
                }
            }
 
        }
    }
}
