using System;
using UnityEngine;
using UniRx;

namespace MatchingGame
{
    namespace View
    {
        public interface IARView
        {
            public IObservable<Vector3> TouchPlaneObservable
            {
                get;
            }

            public Vector3 GetPlayerPosition();
        }

    }
}
