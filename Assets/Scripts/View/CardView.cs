using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace MatchingGame
{
    namespace View
    {
        public class CardView : MonoBehaviour
        {
            private Subject<GameObject> _CardTriggerEnterSubject;
            private Subject<GameObject> _CardTriggerExitSubject;

            private bool _On;
            private int _DebugCount;

            // カード接触時のObserver
            // CardView -> 当たり判定があったCardView
            public IObservable<GameObject> CardTriggerEnterObservable => _CardTriggerEnterSubject;

            // カード離脱時のObserver
            // CardView -> 当たり判定があったCardView
            public IObservable<GameObject> CardTriggerExitObservable => _CardTriggerExitSubject;

            void OnTriggerEnter(Collider me)
            {
                // ローカルプレイヤーの当たり判定のみ検出
                if(me.gameObject.tag == "ME")
                {
                    _CardTriggerEnterSubject.OnNext(this.gameObject);
                }
            }
            

            void OnTriggerExit(Collider me)
            {
                // ローカルプレイヤーの当たり判定のみ検出
                if (me.gameObject.tag == "ME")
                {
                    _CardTriggerExitSubject.OnNext(this.gameObject);
                }
            }
            

            public void FaceUp()
            => transform.rotation = Quaternion.Euler(0, 0, 0);
            

            public void FaceDown()
            => transform.rotation = Quaternion.Euler(180, 0, 0);
            

            private void Awake()
            {
                _CardTriggerEnterSubject = new Subject<GameObject>();
                _CardTriggerExitSubject = new Subject<GameObject>();

            }

            private void OnDestroy()
            {
                _CardTriggerEnterSubject.Dispose();
                _CardTriggerExitSubject.Dispose();
            }
        }
    }
}