using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MatchingGame
{
    namespace View
    {
        public class JoinMenberPanelView : MonoBehaviour
        {
            [SerializeField] List<GameObject> _MemberPanels;

            private int _MemberCount = 0;

            public void RemoveAllMember()
            {
                _MemberCount = 0;
                foreach (GameObject gameObject in _MemberPanels)
                {
                    gameObject.SetActive(false);
                }
            }

            public void AddMember(int id, string name)
            {
                _MemberPanels[_MemberCount].transform.Find("IDText").GetComponent<Text>().text = id.ToString();
                _MemberPanels[_MemberCount].transform.Find("NameText").GetComponent<Text>().text = name;
                _MemberPanels[_MemberCount].SetActive(true);
                _MemberCount++;
            }

            public void RemoveMember(int id)
            {
                int removeIndex = int.MaxValue;
                for(int i=0; i < _MemberCount; i++)
                {
                    if(_MemberPanels[i].transform.Find("IDText").GetComponent<Text>().text == id.ToString())
                    {
                        removeIndex = i;
                    }
                }

                // 対象のユーザーが見つからない場合
                if (removeIndex > _MemberCount)
                {
                    return;
                }

                _MemberCount--;
                if (removeIndex < _MemberCount)
                {
                    for (int i = removeIndex; i < _MemberCount; i++)
                    {
                        _MemberPanels[i].transform.Find("IDText").GetComponent<Text>().text = _MemberPanels[i + 1].transform.Find("IDText").GetComponent<Text>().text;
                        _MemberPanels[i].transform.Find("NameText").GetComponent<Text>().text = _MemberPanels[i + 1].transform.Find("NameText").GetComponent<Text>().text;
                    }   
                }
                _MemberPanels[_MemberCount].SetActive(false);
            }


        }
    }
}
