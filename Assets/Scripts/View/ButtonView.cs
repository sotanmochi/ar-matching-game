using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace MatchingGame
{
    namespace View
    {
        public class ButtonView : MonoBehaviour
        {
            private Subject<GameObject> _Subject;

            public IObservable<GameObject> ButtonClickObservable => _Subject;

            // めくる対象のカード
            private CardView _CardView;

            public void Show(CardView cardView)
            {
                _CardView = cardView;
                gameObject.SetActive(true);
            }

            public void PushFaceUpButton()
            => _Subject.OnNext(_CardView.gameObject);
            

            public void Hide()
            => gameObject.SetActive(false);
            

            private void Awake()
            => _Subject = new Subject<GameObject>();
            

            private void OnDestroy()
            => _Subject.Dispose();
        }
    }
}
