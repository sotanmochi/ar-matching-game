using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

namespace MatchingGame
{
    namespace View
    {
        public class PlayerPinOnMapView : MonoBehaviourPunCallbacks
        {

            [SerializeField] Material _OtherPlayerMaterial;

            public IARView Player { get; set; }

            public void Hide()
            {
                GetComponent<MeshRenderer>().enabled = false;
            }

            public void Show()
            {
                GetComponent<MeshRenderer>().enabled = true;
            }

            private void Start()
            {
                if(!photonView.IsMine)
                {
                    GetComponent<Renderer>().material = _OtherPlayerMaterial;
                }
                // 初期状態は不可視
                GetComponent<MeshRenderer>().enabled = false;
            }

            // Update is called once per frame
            void Update()
            {
                if (Player == null)
                    return;

                Vector3 playerPosition = Player.GetPlayerPosition();
                transform.position = new Vector3(playerPosition.x, transform.position.y, playerPosition.z);
                
            }
        }
    }
}