using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace MatchingGame
{
    namespace View
    {
        public class StartButtonView : MonoBehaviour
        {
            [SerializeField] Dropdown _GameModeDropDown;

            private Subject<int> _Subject;

            public IObservable<int> ButtonClickObservable => _Subject;


            public void Show()
            {
                gameObject.SetActive(true);
                _GameModeDropDown.gameObject.SetActive(true);
            }

            public void Hide()
            {
                gameObject.SetActive(false);
                _GameModeDropDown.gameObject.SetActive(false);
            }

            public void PushStartButton()
            => _Subject.OnNext(_GameModeDropDown.value);

            private void Awake()
            => _Subject = new Subject<int>();


            private void OnDestroy()
            => _Subject.Dispose();
        }
    }
}
