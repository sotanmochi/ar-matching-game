using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MatchingGame
{
    namespace View
    {
        public class PlayerFor3DView : MonoBehaviour
        {
            const float SPEED = 0.01f;
            // Start is called before the first frame update
            void Start()
            {

            }

            // Update is called once per frame
            void Update()
            {
                float x = Input.GetAxisRaw("Horizontal");
                float z = Input.GetAxisRaw("Vertical");
                transform.position += new Vector3(x, 0, z) * SPEED;

            }
        }
    }
}
