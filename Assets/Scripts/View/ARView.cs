using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UniRx;

namespace MatchingGame
{
    namespace View
    {
        public class ARView : MonoBehaviour, IARView
        {
            private Subject<Vector3> _Subject;
            // 平面がタッチされた時のオブザーバ
            // Vector3 => タッチされた場所
            public IObservable<Vector3> TouchPlaneObservable => _Subject;

            private ARRaycastManager _RaycastManager;
            private static List<ARRaycastHit> HITS = new List<ARRaycastHit>();

            public Vector3 GetPlayerPosition()
            {
                return transform.Find("AR Camera").position;
            }

            private void Awake()
            {
                _RaycastManager = GetComponent<ARRaycastManager>();
                _Subject = new Subject<Vector3>();
            }

            void Update()
            {
                if (Input.touchCount > 0)
                {
                    Vector2 touchPosition = Input.GetTouch(0).position;
                    if (_RaycastManager.Raycast(touchPosition, HITS, TrackableType.Planes))
                    {
                        // Raycastの衝突情報は距離によってソートされるため、0番目が最も近い場所でヒットした情報となります
                        _Subject.OnNext(HITS[0].pose.position);
                    }
                }
            }

            private void OnDestroy()
            => _Subject.Dispose();
            
        }
    }
}
