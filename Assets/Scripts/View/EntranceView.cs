using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace MatchingGame
{
    namespace View
    {
        public class EntranceView : MonoBehaviour
        {
            private Subject<Unit> _NewButtonSubject;


            // Newボタン押下時のObserver
            // int -> 選択されているドロップダウンの値
            public IObservable<Unit> NewButtonObservable => _NewButtonSubject;

            // Start is called before the first frame update
            void Awake()
            => _NewButtonSubject = new Subject<Unit>();
            
            public void PushNewButton()
            => _NewButtonSubject.OnNext(Unit.Default);
            
            private void OnDestroy()
            => _NewButtonSubject.Dispose();
            
        }
    }
}