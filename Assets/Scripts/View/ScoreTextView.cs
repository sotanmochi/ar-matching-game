using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MatchingGame
{
    namespace View
    {
        public class ScoreTextView : MonoBehaviour
        {
            public void UpdateScore(int score)
            {
                GetComponent<Text>().text = score + "pt";
            }
        }
    }
}
