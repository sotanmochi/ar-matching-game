using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace MatchingGame
{
    namespace View
    {
        public class MapButtonView : MonoBehaviour
        {
            private Subject<Unit> _Subject;

            public IObservable<Unit> ButtonClickObservable => _Subject;

            public void Show()
            => gameObject.SetActive(true);

            public void Hide()
            => gameObject.SetActive(false);

            public void PushMapButton()
            => _Subject.OnNext(Unit.Default);

            private void Awake()
            => _Subject = new Subject<Unit>();


            private void OnDestroy()
            => _Subject.Dispose();
        }
    }
}