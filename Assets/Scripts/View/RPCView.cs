using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UniRx;
using MatchingGame.Model;
using MatchingGame.Common;

namespace MatchingGame
{
    namespace View
    {
        public class RPCView : MonoBehaviourPunCallbacks
        {
            private Subject<int> _ReadySubject;
            private Subject<Deck> _ReceiveDeckSubject;
            private Subject<Trump> _FaceUpSubject;
            private Subject<int> _ConfirmSubject;

            public IObservable<int> ReadyObservable => _ReadySubject;
            public IObservable<Deck> ReceiveDeckObservable => _ReceiveDeckSubject;
            public IObservable<Trump> FaceUpObservable => _FaceUpSubject;
            public IObservable<int> ConfirmObservable => _ConfirmSubject;

            private void Awake()
            {
                _ReadySubject = new Subject<int>();
                _ReceiveDeckSubject = new Subject<Deck>();
                _FaceUpSubject = new Subject<Trump>();
                _ConfirmSubject = new Subject<int>();
            }

            public void Ready()
            {
                photonView.RPC(nameof(RpcReady), RpcTarget.All);
            }

            [PunRPC]
            private void RpcReady(PhotonMessageInfo info)
            {
                _ReadySubject.OnNext(info.Sender.ActorNumber);
            }

            public void SendDeck(Deck deck)
            {
                String trumps = string.Empty;
                foreach (TrumpInfo trumpInfo in deck.Cards)
                {
                    trumps += ((int)trumpInfo.Trump.TrumpType).ToString() + "-" + ((int)trumpInfo.Trump.TrumpNumber).ToString() + ",";
                }
                Debug.Log(trumps);
                photonView.RPC(nameof(RPCReceiveDeck), RpcTarget.Others, deck.Vertical, deck.Horizontal, trumps);
            }

            [PunRPC]
            private void RPCReceiveDeck(int vertical, int horizontal, string trumps)
            {
                List<TrumpInfo> trumpinfos = new List<TrumpInfo>();
                foreach (string trump in trumps.Split(','))
                {
                    if (trump == string.Empty)
                        continue;

                    string[] typeAndNumber = trump.Split('-');
                    TrumpType trumpType = (TrumpType)Enum.ToObject(typeof(TrumpType), int.Parse(typeAndNumber[0]));
                    TrumpNumber trumpNumber = (TrumpNumber)Enum.ToObject(typeof(TrumpNumber), int.Parse(typeAndNumber[1]));
                    trumpinfos.Add(new TrumpInfo(new Trump(trumpType, trumpNumber), null));
                }
                _ReceiveDeckSubject.OnNext(new Deck(trumpinfos.ToArray(), vertical, horizontal));
            }

            public void FaceUp(TrumpProperty trump)
            {
                photonView.RPC(nameof(RPCFaceUp), RpcTarget.Others, (int)trump.TrumpType, (int)trump.TrumpNumber);
            }

            [PunRPC]
            private void RPCFaceUp(int trumpType, int trumpNumber)
            {
                _FaceUpSubject.OnNext(
                    new Trump((TrumpType)Enum.ToObject(typeof(TrumpType), trumpType),
                    (TrumpNumber)Enum.ToObject(typeof(TrumpNumber), trumpNumber))
                    );
            }

            public void Confirm()
            {
                photonView.RPC(nameof(RPCConfirm), RpcTarget.All);
            }

            [PunRPC]
            private void RPCConfirm(PhotonMessageInfo info)
            {
                _ConfirmSubject.OnNext(info.Sender.ActorNumber);
            }

            private void OnDestroy()
            {
                _ReadySubject.Dispose();
                _ReceiveDeckSubject.Dispose();
                _FaceUpSubject.Dispose();
                _ConfirmSubject.Dispose();
            }

        }
    }
}
