using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace MatchingGame
{
    namespace View
    {
        public class NameInputFieldView : MonoBehaviour
        {
            private Subject<string> _Subject;

            public IObservable<string> OnValueChangedObservable => _Subject;

            public void Hide()
            => gameObject.SetActive(false);

            public void Show()
            => gameObject.SetActive(true);

            public void OnValueChanged(string text)
            => _Subject.OnNext(text);

            public void SetText(string text) 
            => this.GetComponent<InputField>().text = text;

            private void Awake()
            => _Subject = new Subject<string>();


            private void OnDestroy()
            => _Subject.Dispose();
        }
    }
}
