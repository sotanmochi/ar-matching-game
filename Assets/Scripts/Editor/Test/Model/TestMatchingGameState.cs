using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using MatchingGame.Model;
using MatchingGame.Common;
public class TestMatchingGameState
{

    [Test]
    public void MatchingGameFunctionCreate_State生成テスト()
    {
        Player[] players = new Player[]{
            new Player("Me", 1)
        };

        MatchingGameState state = MatchingGameFunction.Create(players, GameMode.MODE_2x2);
        // 条件式がtrueだったら成功
        Assert.IsNotNull(state);
    }

    [Test]
    public void CreateRandomTrump_必ずペアが生成されているテスト()
    {
        var modes = new int[] { 
                            4,  //GameMode.MODE_2x2, 
                            16, //GameMode.MODE_4x4, 
                            32, //GameMode.MODE_4x8, 
                            52  //GameMode.MODE_4x13
                        };

         Enumerable.Range(0, 100).ToList().ForEach(i =>  // 以下を100回繰り返しと同等
            modes.Select(i => ModelUtils.CreateRandomTrump(i)) // 全モードでランダムデッキ作成
                 .Select(j => j.GroupBy(k => k.TrumpNumber)) // 各デッキで番号グループに分ける
                 .ToList() // 各デッキのInstantiate & 次のForEachのため
                 .ForEach(l => Assert.IsTrue( // 各デッキでチェック
                            l.All(group => group.Count() % 2 == 0 ) // 番号グループに含まれる数が偶数であること
                                ))　
            );
    }

    [Test]
    public void CraetePlayers_正常にプレイヤーオブジェクトが作成されるか()
    {
        var created = CreatePlayersRnd();
        // チェック
        Assert.IsInstanceOf<Player[]>(created.Item2); //インスタンスが合っているか
        Assert.IsTrue(created.Item1.Count() == created.Item2.Count()); // プレイヤーの数があっているか
        created.Item2.GroupBy(g => g.PlayerId).ToList().ForEach(g => Assert.IsTrue(g.Count() == 1)); // IDがかぶってないか(名前はかぶっても何とかなる)
        Assert.IsTrue(IsMatchAllName(created.Item2, created.Item1)); // 入力された名前でユーザーが生成されているか
    }

    private (string[], Player[]) CreatePlayersRnd()
    {
        var rnd = new System.Random();
        // ランダム人数で16文字のランダムな文字列を生成
        var playerStrings = Enumerable.Range(1, rnd.Next(3, 10))
                                      .Select(_ => RandomStrGenerate(rnd, 16))
                                      .ToArray();
        //playerStrings.ToList().ForEach(s => Debug.Log(s));
        var players = MatchingGameFunction.CreatePlayers(playerStrings);
        return (playerStrings, players);
    }

    private string RandomStrGenerate(System.Random rnd, int length) 
    {
        const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*)_+{}";
        return new string(Enumerable.Repeat(chars, length).Select(s => s[rnd.Next(s.Length)]).ToArray());
    }

    private bool IsMatchAllName(Player[] players, string[] playerNames) 
         // 名前配列の先頭から、プレイヤー配列の名前の合致するものを引いていく => 2つの配列が同時に0になれば、プレイヤー名がすべて使われている
         => !players.Any() && !playerNames.Any() ? true 
          : !players.Any() && playerNames.Any() ? false // 名前配列に残っている
          : players.Any() && !playerNames.Any() ? false // プレイヤー配列に残っている
          : IsMatchAllName( // 次のチェック
                players.Where(p => p.PlayerName != playerNames.First()).ToArray(),
                playerNames.Where(n => n != playerNames.First()).ToArray()
            );

    [Test]
    public void UpdateDeck_期待するデッキ状態になっているか()
    {
        foreach( var _ in Enumerable.Range(0, 100)) {
            foreach (var mode in Enum.GetValues(typeof(GameMode)).Cast<GameMode>()) {
                var players = CreatePlayersRnd().Item2; 
                var rnd = new System.Random();
                var state = MatchingGameFunction.Create(players, mode);
                var trump1st = state.Deck.Cards[rnd.Next(state.Deck.Cards.Count())].Trump;
                // Debug.Log("1st Number:" + trump1st.TrumpNumber + "Type:" + trump1st.TrumpType);
                // Player0 でFaceUpできること。
                var after1stState = MatchingGameFunction.FaceUp(state, players[0], trump1st);

                // Turnのチェック
                Assert.IsTrue(after1stState.Turn.TryNum == TryNum.TRY_2ND);
                Assert.IsTrue(after1stState.Turn.Player.PlayerId == players[0].PlayerId);
                Assert.IsTrue(after1stState.Turn.Player.PlayerName == players[0].PlayerName);
                Assert.NotNull(after1stState.Turn.Trump);
                Assert.IsTrue(after1stState.Turn.Trump?.TrumpNumber == trump1st.TrumpNumber);
                Assert.IsTrue(after1stState.Turn.Trump?.TrumpType == trump1st.TrumpType);

                // Player0 でFaceUpできること。
                var remainPairable = after1stState.Deck.Cards.Where(c => c.Trump.TrumpType != trump1st.TrumpType && c.Trump.TrumpNumber == trump1st.TrumpNumber); // デッキ中にペアになれるもの
                var trump2nd = remainPairable.ToArray()[rnd.Next(remainPairable.Count())].Trump; // 2手目をランダムで決定
                // Debug.Log("2nd Number:" + trump2nd.TrumpNumber + "Type:" + trump2nd.TrumpType);
                var after2ndState = MatchingGameFunction.FaceUp(after1stState, players[0], trump2nd); // 必ずペアになっている
            
                // Turnのチェック
                Assert.IsTrue(after2ndState.Turn.TryNum == TryNum.TRY_1ST);
                Assert.IsTrue(after2ndState.Turn.Player.PlayerId == players[0].PlayerId);
                Assert.IsTrue(after2ndState.Turn.Player.PlayerName == players[0].PlayerName);
                Assert.IsNull(after2ndState.Turn.Trump);

                // 新デッキ中は必ずペアになっているものが1組 
                var pairInNewDeck = after2ndState.Deck.Cards.Where(c => c.Player != null);
                //pairInNewDeck.ToList().ForEach(c => Debug.Log("Number:" + c.Trump.TrumpNumber + "Type:" + c.Trump.TrumpType));
                Assert.IsTrue(pairInNewDeck.Count() == 2);
                Assert.IsTrue(pairInNewDeck
                            .Select(c => c.Trump)
                            .Concat(new Trump[]{trump1st, trump2nd})
                            .GroupBy(c => c.TrumpType)
                            .All(g => g.Count() == 2));
            }
        }
    }
}
