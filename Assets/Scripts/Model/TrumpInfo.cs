using System.Collections;
using System.Collections.Generic;
using MatchingGame.Common;

namespace MatchingGame
{
    namespace Model
    {
        /// <summary>
        /// カード構造体
        /// カード情報と手札になった人の情報を保持する
        /// </summary>
        /// <param name="trump">Trump</param>
        /// <param name="player">Player</param>
        public struct TrumpInfo
        {
            public Trump Trump { get; }
            public Player? Player { get; }
            public TrumpInfo(
                Trump trump,
                Player? player
            )
            {
                Trump = trump;
                Player = player;
            }
        }
    }
}
