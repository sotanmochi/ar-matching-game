using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MatchingGame
{
    namespace Model
    {
        public struct Result
        {
            public Player Player { get; }
            public int Score { get; }
            public Result (Player player, int score)
            {
                Player = player;
                Score = score;
            }
        }
    }
}