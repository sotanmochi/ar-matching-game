using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using MatchingGame.Common;

namespace MatchingGame
{
    namespace Model 
    {
        static public class ModelUtils 
        {
            /// <summary>
            /// 同じ数値が必ず１ペア以上になるトランプ配列を生成する
            /// </summary>
            /// <param name="trumpNum">希望枚数(偶数である必要がある＆13*4を超えない)</param>
            /// <returns></returns>
            static public Trump[] CreateRandomTrump(int trumpNum)
            {
                if (trumpNum % 2 == 1) {
                    throw new ArgumentException("引数は奇数である必要があります");
                }
                if (trumpNum > (13 * 4))
                {
                    throw new ArgumentException("トランプの最大枚数を超えています");
                }
                TrumpType[] allTrumpType = new TrumpType[]{
                                                TrumpType.CLOVER,
                                                TrumpType.SPADE,
                                                TrumpType.DIAMOND,
                                                TrumpType.HEART
                                                };
                TrumpNumber[] allTrumpNumber = new TrumpNumber[]{
                                                    TrumpNumber.TWO,
                                                    TrumpNumber.THREE,
                                                    TrumpNumber.FOUR,
                                                    TrumpNumber.FIVE,
                                                    TrumpNumber.SIX,
                                                    TrumpNumber.SEVEN,
                                                    TrumpNumber.EIGHT,
                                                    TrumpNumber.NINE,
                                                    TrumpNumber.TEN,
                                                    TrumpNumber.JACK,
                                                    TrumpNumber.QUEEN,
                                                    TrumpNumber.KING,
                                                    TrumpNumber.ACE,
                                                };

                return CreteTrumpRec(
                           (from t in allTrumpType
                            from n in allTrumpNumber
                            select new Trump(t, n)).ToArray(),
                            new Random(),
                            trumpNum).ToArray();

            }

            /// <summary>
            /// Trump山からランダムに2枚ずつ引いていき、それをリストにしていく作業を再帰的に繰り返す
            /// </summary>
            /// <param name="trumpState">現在の残りTrump配列状態</param>
            /// <param name="rnd">ランダム生成機</param>
            /// <param name="remainNum">トランプ残り枚数</param>
            /// <returns></returns>
            static private IEnumerable<Trump> CreteTrumpRec(Trump[] trumpState, Random rnd, int remainNum)
            {
                if (remainNum < 0)
                {
                    throw new ArgumentException("0以上の自然数である必要があります");
                }
                else if (remainNum == 0)
                {
                    return new Trump[] { };
                }
                
                
                // 1枚ランダムに引く
                Trump drawTrump = trumpState[rnd.Next(trumpState.Length)];
                Trump[] newTrumpState = trumpState.Where(t => !t.Equals(drawTrump)).ToArray();

                // 引いたトランプと同じ番号のトランプをランダムにひく
                Trump[] drawTrumpSameNumbers = newTrumpState.Where(t => t.TrumpNumber.Equals(drawTrump.TrumpNumber)).ToArray();
                Trump drawTrumpSameNumber = drawTrumpSameNumbers[rnd.Next(drawTrumpSameNumbers.Length)];


                return (new Trump[] { drawTrump, drawTrumpSameNumber })
                        .Concat(CreteTrumpRec(
                            newTrumpState.Where(t => !t.Equals(drawTrumpSameNumber)).ToArray(),
                            rnd,
                            remainNum - 2
                            )); 
            }

            static public string LogStringMatchingGameState(MatchingGameState state)
            {
                var player = new string[] {
                    "Players:",
                };
                var players = state.Players.Select(p =>
                    "    PlayerID:                  " + p.PlayerId.ToString() + "\n"
                  + "    PlayerName:                " + p.PlayerName.ToString());
                var deck = new string[] {
                    "Deck:",
                };
                var cards = state.Deck.Cards.Select(c =>
                    "    Trump.TrumpNumber:         " + c.Trump.TrumpNumber.ToString() + "\n" +
                    "    Trump.TrumpType:           " + c.Trump.TrumpType.ToString() + "\n" +
                    "    Trump.PlayerId:            " + c.Player?.PlayerId.ToString() + "\n" +
                    "    Trump.PlayerName:          " + c.Player?.PlayerName.ToString()
                );
                var result = new string[] {
                    "State Hash:                    " + state.GetHashCode().ToString(),
                    "Deck: ", 
                    "    Deck_vertical:             " + state.Deck.Vertical.ToString(),
                    "    Deck_horizontal:           " + state.Deck.Horizontal.ToString(),
                    "Turn: ",
                    "    Turn_Trump_Number:         " + state.Turn.Trump?.TrumpNumber.ToString(),
                    "    Turn_Trump_Type:           " + state.Turn.Trump?.TrumpType.ToString(),
                    "    Turn_Player_PlayerName:    " + state.Turn.Player.PlayerName.ToString(),
                    "    Turn_Player_PlayerID:      " + state.Turn.Player.PlayerId.ToString(),
                    "    Turn_Hand:                 " + state.Turn.TryNum.ToString(),
                };
                return string.Join("\n", result.Concat(player).Concat(players).Concat(deck).Concat(cards).ToArray());
            }
        }
    }
}
