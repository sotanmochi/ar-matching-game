using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using MatchingGame.Common;

namespace MatchingGame
{
    namespace Model
    {
        static public class MatchingGameFunction
        {
            static public MatchingGameState Create(
                Player[] players,
                GameMode gameMode
            )
            => new MatchingGameState(
                players,
                new Turn(players[0], TryNum.TRY_1ST, null),
                CreateDeck(gameMode)
                );

            static public MatchingGameState Create(
                Player[] players,
                Deck deck
            ) 
            => new MatchingGameState(
                players,
                new Turn(players[0], TryNum.TRY_1ST, null),
                deck
                );

            static public Player[] CreatePlayers(string[] playerNames) 
            =>  Enumerable.Range(0, playerNames.Count())
                .Zip(playerNames, (id, name) => new Player(name, id))
                .ToArray();

            static public bool CanFaceUp(MatchingGameState state, int playerId)
            => state.Turn.Player.PlayerId == playerId;
            

            static public MatchingGameState FaceUp(MatchingGameState state, Player player, Trump trump)
            => (state.Turn.TryNum == TryNum.TRY_1ST) 
                ? FaceUpTry1st(state, player, trump) 
                : FaceUpTry2nd(state, player, trump);            

            static public bool IsEndGame(MatchingGameState state)
            => state.Deck.Cards.All(c => c.Player != null);            

            static public bool Is2ndTry(MatchingGameState state)
            => state.Turn.TryNum.Equals(TryNum.TRY_2ND);            

            static public Deck GetDeck(MatchingGameState state)
            => state.Deck;

            static private Deck CreateDeck(GameMode mode)
            {
                switch (mode)
                {
                    case GameMode.MODE_2x2: return CreateDeckInternal(2, 2);
                    case GameMode.MODE_4x4: return CreateDeckInternal(4, 4);
                    case GameMode.MODE_4x8: return CreateDeckInternal(4, 8);
                    case GameMode.MODE_4x13: return CreateDeckInternal(4, 13);
                    default: throw new InvalidProgramException("場合が網羅されていません");
                        
                };
            }

            static private Deck CreateDeckInternal(int vertical, int horizontal)
            {   
                Trump[] cards = ModelUtils.CreateRandomTrump(horizontal * vertical);
                Random rnd = new Random();
                return new Deck(
                    cards
                        .Select(c => new TrumpInfo(c, null))
                        .OrderBy(i => rnd.Next()).ToArray(), // shuffle処理
                    vertical, horizontal);
            }

            static private MatchingGameState UpdateTurn(MatchingGameState state, Turn turn)
            => new MatchingGameState(
                state.Players,
                turn,
                state.Deck
               );
        

            static private MatchingGameState FaceUpTry1st(MatchingGameState state, Player player, Trump trump)
            => UpdateTurn(
                state,
                new Turn(player, TryNum.TRY_2ND, trump)
               );
        

            static private MatchingGameState FaceUpTry2nd(MatchingGameState state, Player player, Trump trump)
            => (state.Turn.Trump == null) ? // 状態不正
                throw new ArgumentException("TurnにTrumpが設定されていません")
                : (state.Turn.Trump?.TrumpNumber == trump.TrumpNumber) ? // アタリ
                UpdateTurn(
                        UpdateDeck(state, player, trump), // Deckを更新する
                        new Turn(
                            player,
                            TryNum.TRY_1ST,
                            null)
                        )
                : // ハズレ
                UpdateTurn(
                        state,
                        new Turn(
                            GetNextPlayer(state.Players, player),
                            TryNum.TRY_1ST,
                            null)
                        );
                
            static private Player GetNextPlayer(Player[] players, Player currentPlayer)
            {
                // currentPlayerがリストの最後の人の場合を考慮し、リストを２倍にする
                var concatPlayers = players.Concat(players);
                var currentPlayerIndex = concatPlayers.Select((s, index) => new { s, index })
                                         .Where(x => x.s.Equals(currentPlayer))
                                         .Select(x => x.index)
                                         .First();

                return concatPlayers.ToArray()[currentPlayerIndex + 1];
            }

            static private MatchingGameState UpdateDeck(MatchingGameState state, Player player, Trump trump)
            => new MatchingGameState(
                        state.Players,
                        state.Turn,
                        new Deck(
                            state.Deck.Cards
                                .Select(c => c.Trump.Equals(trump) ? new TrumpInfo(c.Trump, player) : c)
                                .Select(c => c.Trump.Equals(state.Turn.Trump) ? new TrumpInfo(c.Trump, player) : c)
                                .ToArray(),
                            state.Deck.Horizontal,
                            state.Deck.Vertical
                         )
                );
            
            static public IEnumerable<Result> GetResutls (MatchingGameState state) 
            => state.Players.Select(p => new Result(p, state.Deck.Cards.Where(c => c.Player?.PlayerId == p.PlayerId).Count() / 2));

            static public int GetPlayerScore(MatchingGameState state, int playerId)
            => state.Deck.Cards.Where(c => c.Player?.PlayerId == playerId).Count() / 2;


        }
    }
}
