using System.Collections;
using System.Collections.Generic;

namespace MatchingGame
{
    namespace Model
    {
        /// <summary>Player構造体</summary>
        public struct Player
        {
            public string PlayerName { get; }
            public int PlayerId { get; }

            /// <summary>Player構造体コンストラクタ</summary>
            /// <param name="playerName">PlayerName</param>
            /// <param name="playerId">PlayerID</param>
            public Player(string playerName, int playerId)
            {
                PlayerName = playerName;
                PlayerId = playerId;
            }
        }
    }
}