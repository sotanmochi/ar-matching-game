using System.Collections;
using System.Collections.Generic;


namespace MatchingGame
{
    namespace Model
    {
        public struct Deck
        {
            public TrumpInfo[] Cards { get; }
            public int Horizontal { get; }
            public int Vertical { get; }
            public Deck(TrumpInfo[] cards, int vertical, int horizontal)
            {
                Cards = cards;
                Vertical = vertical;
                Horizontal = horizontal;
            }
        }

        public struct MatchingGameState
        {
            public Player[] Players { get; }
            public Turn Turn { get; }
            public Deck Deck { get; }

            public MatchingGameState(
                Player[] players,
                Turn turn,
                Deck deck
            )
            {
                Players = players;
                Turn = turn;
                Deck = deck;
            }
        }
    }
}
