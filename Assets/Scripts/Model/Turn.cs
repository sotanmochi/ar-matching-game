﻿using System;
using MatchingGame.Common;

namespace MatchingGame
{
    namespace Model
    {
        public enum TryNum
        {
            TRY_1ST,
            TRY_2ND,
        };

        /// <summary>Turn情報構造体</summary>
        public struct Turn
        {
            public TryNum TryNum { get; }
            public Player Player { get; }
            public Trump? Trump { get; }

            /// <summary>Turn情報構造体コンストラクタ</summary>
            /// <param name="player">プレイヤー</param>
            /// <param name="tryNum">試行回数</param>
            /// <param name="trump">1手目の手札</param>
            public Turn(Player player, TryNum tryNum, Trump? trump)
            {
                TryNum = tryNum;
                Player = player;
                Trump = trump;
            }
        }
    }
}

